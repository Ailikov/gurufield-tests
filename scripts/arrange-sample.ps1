﻿Set-StrictMode -Version Latest

$executablePath = "..\AlgoTests.Cli\bin\Debug\netcoreapp2.2\AlgoTests.Cli.dll"

# check files presense 
if (-not ($executablePath|Test-path)) {

    Write-Host "No executable file was found:"$executablePath"."
    Write-Host "Try to build it first."

    Exit 1
}

$key_value = "yellow"
$values = "red,green,yellow,blue,purple"

& "dotnet" $executablePath shift -k $key_value -v $values
