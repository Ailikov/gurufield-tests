using System.Collections.Generic;
using AlgoTests.Cli;
using AlgoTests.Cli.ArrayArrangeScenarios;
using FluentAssertions;
using Xunit;

namespace AlgoTests.Playground
{
    public class RearrangeTests
    {
        [Fact]
        public void SingleItemArrayIsTolerated()
        {
            //var array = new List<string> {"red", "green", "yellow", "blue", "purple"};
            
            var array = new List<string> {"red"};
            
            // act
            array.ShiftListElements(0);
            
            // assert
            array.Should().BeEquivalentTo(new [] { "red" });
        }
        
        [Fact]
        public void TwoItemsArrayIsIntactAfterShiftFrom0()
        {
            var array = new List<string> {"red", "green"};
            
            // act
            array.ShiftListElements(0);
            
            // assert
            array.Should().BeEquivalentTo(new [] { "red", "green" });
        }
        
        [Fact]
        public void TwoItemsArrayIsShiftedBy1()
        {
            var array = new List<string> {"red", "green"};
            
            // act
            array.ShiftListElements(1);
            
            // assert
            array.Should().BeEquivalentTo(new [] { "green", "red" });
        }
        
        [Fact]
        public void ThreeItemsArrayIsIntactAfterShiftFrom0()
        {
            var array = new List<string> {"red", "green", "blue"};
            
            // act
            array.ShiftListElements(0);
            
            // assert
            array.Should().BeEquivalentTo(new [] { "red", "green", "blue" });
        }
        
        [Fact]
        public void ThreeItemsArrayIsShiftedBy1()
        {
            var array = new List<string> {"red", "green", "blue"};
            
            // act
            array.ShiftListElements(1);
            
            // assert
            array.Should().BeEquivalentTo(new [] { "green", "blue", "red" });
        }
        
        [Fact]
        public void ThreeItemsArrayIsShiftedBy2()
        {
            var array = new List<string> {"red", "green", "blue"};
            
            // act
            array.ShiftListElements(2);
            
            // assert
            array.Should().BeEquivalentTo(new [] { "blue", "red", "green" });
        }
        
        
    }
}