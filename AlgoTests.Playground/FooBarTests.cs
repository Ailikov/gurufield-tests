using System.Globalization;
using AlgoTests.Cli;
using AlgoTests.Cli.FooBarScenarios;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace AlgoTests.Playground
{
    public class FooBarTests
    {
        private readonly ITestOutputHelper output;

        public FooBarTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void GenerateSequenceWithSpecialCounters()
        {
            var sut = new FooBarSequence();

            while (sut.MoveNext())
            {
                sut.Index.Should().BeGreaterThan(FooBarSequence.MIN_SEQUENCE_BOUNDARY);
                sut.Index.Should().BeLessThan(FooBarSequence.MAX_SEQUENCE_BOUNDARY);
                
                var isFoo = sut.Index % 3 == 0;
                var isBar = sut.Index % 5 == 0;
                
                var message = sut.Current;
                if (isFoo)
                {
                    message.Should().Contain("Foo");
                }
                else
                {
                    message.Should().NotContain("Foo");
                }
                
                if (isBar)
                {
                    message.Should().Contain("Bar");
                }
                else
                {
                    message.Should().NotContain("Bar");
                }

                if (!isFoo && !isBar)
                {
                    message.Should().Be(sut.Index.ToString(CultureInfo.InvariantCulture));
                }
                
                output.WriteLine(message);
            }
        }
    }
}