using System;
using System.Collections.Generic;

namespace AlgoTests.Cli.ArrayArrangeScenarios
{
    public static class ListShiftingExtensions
    {
        public static void ShiftListElements(this List<string> array, int newStartingIndex)
        {
            if (newStartingIndex < 0 || newStartingIndex >= array.Count)
                throw new IndexOutOfRangeException(nameof(newStartingIndex) + "should be inside " + nameof(array) + "dimensions");
            
            var leftOffset = newStartingIndex;
            var rightOffset = array.Count - newStartingIndex;

            var cycleStart = newStartingIndex;

            while (cycleStart < array.Count)
            {
                var pointer = cycleStart - leftOffset;
                var srcElement = array[cycleStart];
                
                array[cycleStart] = array[pointer];
                
                while (pointer <= cycleStart)
                {
                    // swap elements
                    var tmp = array[pointer];
                    array[pointer] = srcElement;
                    srcElement = tmp;
                    
                    pointer += rightOffset;
                }

                cycleStart++;
            }
        }   
    }
}