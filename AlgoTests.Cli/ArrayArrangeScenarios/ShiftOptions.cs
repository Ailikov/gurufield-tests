using System.Collections.Generic;
using CommandLine;

namespace AlgoTests.Cli.ArrayArrangeScenarios
{
    [Verb("shift", HelpText = "Shift array around given value.")]
    public class ShiftOptions
    {
        [Option('k', "key-value", Required = true, HelpText = "Value of a new list starting element.")]
        public string KeyValue { get; set; }

        [Option('v', "values", Required = true, Separator = ',', HelpText = "Comma-separated list of array values.")]
        public IEnumerable<string> Values { get; set; }
    }
}