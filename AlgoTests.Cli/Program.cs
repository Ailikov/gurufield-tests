﻿using System;
using System.IO;
using System.Linq;
using AlgoTests.Cli.ArrayArrangeScenarios;
using AlgoTests.Cli.FooBarScenarios;
using CommandLine;

namespace AlgoTests.Cli
{
    class Program
    {
        static int Main(string[] args)
        {
            try
            {
                return Parser.Default.ParseArguments<FooBarOptions, ShiftOptions>(args)
                             .MapResult(
                                 (FooBarOptions opts) => RunFooBar(opts, Console.Out),
                                 (ShiftOptions opts) => RunShift(opts, Console.Out),
                                 errs =>
                                 {
                                     Console.Out.WriteLine("Errors at arguments parsing stage:");
                                     foreach (var error in errs)
                                     {
                                         Console.Out.WriteLine(error.ToString());
                                     }

                                     return 1;
                                 });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return 1;
            }
        }

        public static int RunFooBar(FooBarOptions opts, TextWriter writer)
        {
            writer.WriteLine("Generating 1-2-Foo-4-Bar-.. sequence");
            
            var generator = new FooBarSequence();

            while (generator.MoveNext())
            {
                writer.WriteLine(generator.Current);
            }
            
            return 0;
        }

        private static int RunShift(ShiftOptions opts, TextWriter writer)
        {
            var values = opts.Values.ToList();
            
            var index = values.IndexOf(opts.KeyValue);

            if (index < 0)
            {
                writer.WriteLine("Given key '{0}' was not found in a sequence.", opts.KeyValue);
                return 1;
            }
            
            writer.WriteLine("Given key '{0}' was found at '{1}' position", opts.KeyValue, index);
            writer.WriteLine("Rearranging array values...");

            values.ShiftListElements(index);
            
            writer.Write("Old elements order: ");
            writer.WriteLine(String.Join(", ", opts.Values));
            writer.Write("New elements order: ");
            writer.WriteLine(String.Join(", ", values));
            
            return 0;
        }
    }
}