using System.Globalization;

namespace AlgoTests.Cli.FooBarScenarios
{
    public class FooBarSequence
    {
        public const int MIN_SEQUENCE_BOUNDARY = 0;
        public const int MAX_SEQUENCE_BOUNDARY = 101;

        private int nextFooIndex;
        private int nextBarIndex;

        public FooBarSequence()
        {
            Reset();
        }

        public int Index { get; private set; }

        public bool MoveNext()
        {
            Index++;

            if (Index >= MAX_SEQUENCE_BOUNDARY)
                return false;

            var isFoo = (nextFooIndex == Index);
            var isBar = (nextBarIndex == Index);

            if (isFoo)
                nextFooIndex += 3;

            if (isBar)
                nextBarIndex += 5;

            Current = FormatMessage(Index, isFoo, isBar);

            return true;
        }

        private static string FormatMessage(int index, bool isFoo, bool isBar)
        {
            if (isFoo && isBar)
            {
                return "Foo, Bar";
            }
            else if (isFoo)
            {
                return "Foo";
            }
            else if (isBar)
            {
                return "Bar";
            }

            return index.ToString(CultureInfo.InvariantCulture);
        }

        private void Reset()
        {
            this.nextFooIndex = 3;
            this.nextBarIndex = 5;

            this.Index = MIN_SEQUENCE_BOUNDARY;
        }

        public string Current { get; private set; }
    }
}